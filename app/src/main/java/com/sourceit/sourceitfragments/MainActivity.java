package com.sourceit.sourceitfragments;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportFragmentManager() //получаем менеджер
                .beginTransaction() //начинаем транзакцию
                .replace(R.id.container, MyGeniusFragment.newInstance("great")) //замещаем фрагмент в контейнере container
                .commit(); //применяем изменения
    }
}
